<?php

namespace PanelSsh\Core\Exports;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Schema\Builder as SchemaBuilder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

abstract class BaseExport implements FromQuery, ShouldAutoSize, WithHeadings, WithMapping, WithStyles
{
    use Exportable;

    public $query;

    abstract public function headings(): array;

    abstract public function map($row): array;

    public function query()
    {
        if ($this->query instanceof EloquentBuilder ||
            $this->query instanceof QueryBuilder ||
            $this->query instanceof SchemaBuilder) {
            return $this->query;
        }

        return (new $this->query)->newQuery();
    }

    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
}
