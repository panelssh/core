<?php

namespace PanelSsh\Core\Exports;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Schema\Builder as SchemaBuilder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class MultipleExport implements WithMultipleSheets
{
    use Exportable;

    public $query;

    public $exports;

    public function sheets(): array
    {
        $query = $this->getQuery()->get();

        $sheets = [];

        foreach ($this->exports as $class => $export) {
            $sheetClass = new $class;

            if (isset($export)) {
                $sheets[] = $sheetClass->setCollection($query->pluck($export)->collapse());
            } else {
                $sheets[] = $sheetClass->setCollection($query);
            }

        }

        return $sheets;
    }

    public function getQuery()
    {
        if ($this->query instanceof EloquentBuilder ||
            $this->query instanceof QueryBuilder ||
            $this->query instanceof SchemaBuilder) {
            return $this->query;
        }

        return (new $this->query)->newQuery();
    }

    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    public function setExports($exports)
    {
        $this->exports = $exports;

        return $this;
    }
}
