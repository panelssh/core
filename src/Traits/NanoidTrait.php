<?php

namespace PanelSsh\Core\Traits;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_ext
 * @mixin Model
 */
trait NanoidTrait
{
    protected static function bootNanoidTrait()
    {
        static::creating(function ($model) {
            $model->id_ext = nanoid();
        });
    }
}
