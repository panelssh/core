<?php

namespace PanelSsh\Core\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property bool $is_active
 * @method static $this|Builder active()
 * @method static $this|Builder inactive()
 * @mixin Model
 */
trait StatusTrait
{
    protected static function bootStatus()
    {
        static::creating(function ($model) {
            if (is_null($model->is_active)) {
                $model->is_active = false;
            }
        });
    }

    public function scopeActive(Builder $query)
    {
        return $query->where('is_active', true);
    }

    public function scopeInactive(Builder $query)
    {
        return $query->where('is_active', false);
    }

    public function toggleStatus($otherData = []): void
    {
        $this->update(array_merge($otherData, [
            'is_active' => !$this->is_active,
        ]));
    }
}
