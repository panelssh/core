<?php

namespace PanelSsh\Core\Controllers;

use Illuminate\Support\Str;
use PanelSsh\Core\Exports\MultipleExport;

/**
 * @property string $excelExporter
 */
trait ExportController
{
    public function export()
    {
        if ($this->excelExporter()) {
            if (is_array($this->excelExporter())) {
                $export =  (new MultipleExport)
                    ->setQuery($this->model()->with($this->modelRelations))
                    ->setExports($this->excelExporter());
            } else {
                $export = (new $this->excelExporter())
                    ->setQuery($this->model()->with($this->modelRelations));
            }

            return $export->download(Str::snake($this->title()) . '_' . date('YmdHi') . '.xlsx');
        }

        return response()->json(['message' => __('dashboard.export.error')], 403);
    }

    protected function excelExporter()
    {
        return $this->getExcelExporter();
    }

    protected function getExcelExporter()
    {
        return $this->excelExporter ?? null;
    }

    protected function setExcelExporter(string $excelExporter)
    {
        $this->excelExporter = $excelExporter;

        return $this;
    }
}
