<?php

namespace PanelSsh\Core\Controllers;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

/**
 * @property FormRequest $storeFormRequest
 */
trait StoreController
{
    public function store()
    {
        $request = $this->storeFormRequest();

        DB::beginTransaction();
        try {
            $this->performBeforeStore($request);

            $data = $this->performStore($request);

            $this->performAfterStore($request, $data);

            DB::commit();
        } catch (\Exception $e) {
            report($e);

            DB::rollBack();

            throw_if(config('app.debug'), $e);

            return redirect()->back()->withInput($request->input());
        }

        return redirect()->route("{$this->route}.index");
    }

    public function storeFormRequest()
    {
        if (property_exists($this, 'storeFormRequest')) {
            return app($this->storeFormRequest);
        }

        return app('request');
    }

    public function getStoreFormRequest()
    {
        return $this->storeFormRequest;
    }

    public function setStoreFormRequest($storeFormRequest)
    {
        $this->storeFormRequest = $storeFormRequest;

        return $this;
    }

    protected function performBeforeStore($request)
    {
    }

    protected function performStore($request)
    {
        return $this->model()->create($request->input());
    }

    protected function performAfterStore($request, $model)
    {
    }
}
