<?php

namespace PanelSsh\Core\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @property string $statusDeactivateIdentifier
 * @property string $statusDeactivateColumn
 */
trait StatusDeactivateController
{
    public function statusDeactivate(Request $request)
    {
        abort_unless($request->ajax(), 404);

        $request->validate(['ids' => ['required', 'array']]);

        DB::beginTransaction();
        try {
            $this->performBeforeStatusDeactivate($request);

            $this->performStatusDeactivate($request);

            $this->performAfterStatusDeactivate($request);

            DB::commit();
        } catch (\Exception $e) {
            report($e);

            DB::rollBack();

            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json([
            'status' => 'ok',
            'message' => __('dashboard.statusDeactivate.success', ['title' => $this->title()]),
        ]);
    }

    protected function statusDeactivateIdentifier()
    {
        if (property_exists($this, 'statusDeactivateIdentifier')) {
            return $this->statusDeactivateIdentifier;
        }

        return $this->getStatusDeactivateIdentifier();
    }

    protected function getStatusDeactivateIdentifier()
    {
        return $this->statusDeactivateIdentifier ?? 'id_ext';
    }

    protected function setStatusDeactivateIdentifier(string $statusDeactivateIdentifier)
    {
        $this->statusDeactivateIdentifier = $statusDeactivateIdentifier;

        return $this;
    }

    protected function statusDeactivateColumn()
    {
        if (property_exists($this, 'statusDeactivateColumn')) {
            return $this->statusDeactivateColumn;
        }

        return $this->getStatusDeactivateColumn();
    }

    protected function getStatusDeactivateColumn()
    {
        return $this->statusDeactivateColumn ?? 'is_active';
    }

    protected function setStatusDeactivateColumn(string $statusDeactivateColumn)
    {
        $this->statusDeactivateColumn = $statusDeactivateColumn;

        return $this;
    }

    protected function performBeforeStatusDeactivate($request)
    {
    }

    protected function performStatusDeactivate($request)
    {
        $this->model()
            ->whereIn($this->statusDeactivateIdentifier(), $request->ids)
            ->update([$this->statusDeactivateColumn() => false]);
    }

    protected function performAfterStatusDeactivate($request)
    {
    }
}
