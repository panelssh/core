<?php

namespace PanelSsh\Core\Controllers;

/**
 * @property string $createLayout
 */
trait CreateController
{
    public function create()
    {
        $title = data_get($this->titleAlt(), 'create', $this->title());

        return $this
            ->addVars([
                'forms' => $this->createForms(),
                'form_title' => $title,
                'form_method' => 'post',
                'form_action' => route("{$this->route}.store"),
                'button_reset' => true,
                'button_create' => true,
            ])
            ->appendBreadcrumbs(['title' => $title])
            ->createRender();
    }

    protected function createForms()
    {
        return [];
    }

    protected function createLayout()
    {
        return $this->getCreateLayout() ?: 'dashboard.pages.create';
    }

    protected function getCreateLayout()
    {
        if (property_exists($this, 'createLayout')) {
            return $this->createLayout;
        }

        return null;
    }

    protected function setCreateLayout(string $createLayout)
    {
        $this->createLayout = $createLayout;

        return $this;
    }

    protected function createRender()
    {
        return view($this->createLayout(), $this->vars());
    }
}
