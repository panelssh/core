<?php

namespace PanelSsh\Core\Controllers;

/**
 * @property string $showLayout
 */
trait ShowController
{
    public function show()
    {
        $title = data_get($this->titleAlt(), 'show', $this->title());

        return $this
            ->addVars([
                'forms' => $this->showForms($this->data()),
                'form_title' => $title,
                'button_edit' => true,
            ])
            ->appendBreadcrumbs(['title' => $title])
            ->showRender();
    }

    protected function showForms($data)
    {
        return [];
    }

    protected function showLayout()
    {
        return $this->getShowLayout() ?: 'dashboard.pages.show';
    }

    protected function getShowLayout()
    {
        if (property_exists($this, 'showLayout')) {
            return $this->showLayout;
        }

        return null;
    }

    protected function setShowLayout(string $showLayout)
    {
        $this->showLayout = $showLayout;

        return $this;
    }

    protected function showRender()
    {
        return view($this->showLayout(), $this->vars());
    }
}
