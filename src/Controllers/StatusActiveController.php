<?php

namespace PanelSsh\Core\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @property string $statusActiveIdentifier
 * @property string $statusActiveColumn
 */
trait StatusActiveController
{
    public function statusActive(Request $request)
    {
        abort_unless($request->ajax(), 404);

        $request->validate(['ids' => ['required', 'array']]);

        DB::beginTransaction();
        try {
            $this->performBeforeStatusActive($request);

            $this->performStatusActivate($request);

            $this->performAfterStatusActive($request);

            DB::commit();
        } catch (\Exception $e) {
            report($e);

            DB::rollBack();

            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json([
            'status' => 'ok',
            'message' => __('dashboard.statusActive.success', ['title' => $this->title()]),
        ]);
    }

    protected function statusActiveIdentifier()
    {
        if (property_exists($this, 'statusActiveIdentifier')) {
            return $this->statusActiveIdentifier;
        }

        return $this->getStatusActiveIdentifier();
    }

    protected function getStatusActiveIdentifier()
    {
        return $this->statusActiveIdentifier ?? 'id_ext';
    }

    protected function setStatusActiveIdentifier(string $statusActiveIdentifier)
    {
        $this->statusActiveIdentifier = $statusActiveIdentifier;

        return $this;
    }

    protected function statusActiveColumn()
    {
        if (property_exists($this, 'statusActiveColumn')) {
            return $this->statusActiveColumn;
        }

        return $this->getStatusActiveColumn();
    }

    protected function getStatusActiveColumn()
    {
        return $this->statusActiveColumn ?? 'is_active';
    }

    protected function setStatusActiveColumn(string $statusActiveColumn)
    {
        $this->statusActiveColumn = $statusActiveColumn;

        return $this;
    }

    protected function performBeforeStatusActive($request)
    {
    }

    protected function performStatusActivate($request)
    {
        $this->model()
            ->whereIn($this->statusActiveIdentifier(), $request->ids)
            ->update([$this->statusActiveColumn() => true]);
    }

    protected function performAfterStatusActive($request)
    {
    }
}
