<?php

namespace PanelSsh\Core\DataTables;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Schema\Builder as SchemaBuilder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable as BaseDataTable;

abstract class DataTable extends BaseDataTable
{
    public $route;

    public $model;

    protected $actions = ['export', 'import'];

    protected $tableClass = 'table-hover';

    protected $autoWidth = false;

    protected $deferRender = true;

    protected $info = true;

    protected $lengthChange = true;

    protected $ordering = true;

    protected $processing = false;

    protected $scrollX = true;

    protected $scrollY = false;

    protected $paging = true;

    protected $searching = true;

    protected $serverSide = true;

    protected $stateSave = false;

    protected $lengthMenu = [10, 25, 50, 100];

    protected $orderIndex = 2;

    protected $orderBy = 'asc';

    protected $pageLength = 10;

    protected $pagingType = '$(window).width() < 768 ? "full" : "full_numbers"';

    abstract protected function getColumns(): array;

    abstract protected function getButtons(): array;

    protected function getDom()
    {
        $tagTable = '<"d-flex justify-content-center"r><"row"<"col-12"t>>';
        $tagButton = '<"d-flex justify-content-end"B>';
        $tagFilter = '<"row my-2"<"col-lg-6"l><"col-lg-6 text-right"f>>';
        $tagInfo = '<"col-lg-6 d-flex justify-content-center justify-content-lg-start"i>';
        $tagPagination = '<"col-lg-6 d-flex justify-content-center justify-content-lg-end"p>';
        $tagNavigation = '<"row my-2"' . $tagInfo . $tagPagination . '>';

        return "{$tagButton}{$tagFilter}{$tagNavigation}{$tagTable}{$tagFilter}{$tagNavigation}{$tagButton}";
    }

    protected function getColumnDefs()
    {
        return [
            [
                'targets' => '_all',
                'className' => 'text-nowrap',
            ],
        ];
    }

    protected function status(bool $status, $url)
    {
        if ($status) {
            $class = 'bg-success';
            $label = 'Active';
        } else {
            $class = 'bg-danger';
            $label = 'Inactive';
        }

        return sprintf(
            '<button type="button" onclick="dashboard.statusAction(\'%s\');" class="btn badge %s">%s</button>',
            $url,
            $class,
            $label
        );
    }

    protected function action($data, $excepts = null)
    {
        $actions = [];

        if (Route::has("{$this->getRoute()}.show")) {
            $actions['show'] = route("{$this->getRoute()}.show", $data->id_ext);
        }

        if (Route::has("{$this->getRoute()}.edit")) {
            $actions['edit'] = route("{$this->getRoute()}.edit", $data->id_ext);
        }

        if (Route::has("{$this->getRoute()}.destroy")) {
            $actions['delete'] = route("{$this->getRoute()}.destroy", $data->id_ext);
        }

        if (Route::has("{$this->getRoute()}.status")) {
            $actions[] = [
                'url' => route("{$this->getRoute()}.status", $data->id_ext),
                'icon' => $data->is_active ? 'fas fa-lock' : 'fas fa-unlock',
                'class' => $data->is_active ? 'btn-warning' : 'bg-teal',
            ];
        }

        if (filled($excepts)) {
            return view('dashboard.partials._actions', Arr::except($actions, $excepts));
        }

        return view('dashboard.partials._actions', $actions);
    }

    public function html()
    {
        return parent::html()
            ->dom($this->getDom())
            ->addTableClass($this->tableClass)
            ->autoWidth($this->autoWidth)
            ->deferRender($this->deferRender)
            ->info($this->info)
            ->lengthChange($this->lengthChange)
            ->ordering($this->ordering)
            ->processing($this->processing)
            ->scrollX($this->scrollX)
            ->scrollY($this->scrollY)
            ->paging($this->paging)
            ->searching($this->searching)
            ->serverSide($this->serverSide)
            ->stateSave($this->stateSave)
            ->lengthMenu($this->lengthMenu)
            ->orderBy($this->orderIndex, $this->orderBy)
            ->pageLength($this->pageLength)
            ->pagingType($this->pagingType)
            ->selectSelector('td:first-child')
            ->selectStyleMulti()
            ->columns($this->getColumns())
            ->columnDefs($this->getColumnDefs())
            ->buttons($this->getButtons())
            ->languagePaginateFirst('<i class="fas fa-angle-double-left"></i>')
            ->languagePaginatePrevious('<i class="fas fa-angle-left"></i>')
            ->languagePaginateNext('<i class="fas fa-angle-right"></i>')
            ->languagePaginateLast('<i class="fas fa-angle-double-right"></i>');
    }

    public function getModel()
    {
        if ($this->model instanceof EloquentBuilder ||
            $this->model instanceof QueryBuilder ||
            $this->model instanceof SchemaBuilder) {
            return $this->model;
        }

        return (new $this->model)->newQuery();
    }

    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    public function getRoute()
    {
        return $this->route;
    }

    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    public function query()
    {
        $query = $this->getModel();

        return $this->applyScopes($query);
    }

    protected static function columnAction()
    {
        return Column::computed('actions')
            ->addClass('text-nowrap text-center')
            ->title('Actions');
    }

    protected static function buttonReload()
    {
        return Button::make()
            ->extend('reload')
            ->text('Reload')
            ->className('btn-primary my-1 my-md-0');
    }

    protected static function buttonExport($url)
    {
        return Button::make()
            ->extend('export')
            ->text('Export')
            ->attr(['data-url' => $url])
            ->className('btn-success my-1 my-md-0');
    }

    protected static function buttonImport($url, $templateUrl)
    {
        return Button::make()
            ->extend('import')
            ->text('Import')
            ->attr(['data-url' => $url, 'data-template-url' => $templateUrl])
            ->className('btn-info my-1 my-md-0');
    }

    protected static function buttonSelectAll()
    {
        return Button::make()
            ->extend('selectAll')
            ->className('btn-secondary my-1 my-md-0');
    }

    protected static function buttonSelectNone()
    {
        return Button::make()
            ->extend('selectNone')
            ->className('btn-secondary my-1 my-md-0');
    }

    protected static function buttonSelectActivate($url)
    {
        return Button::make()
            ->extend('selected')
            ->text('Activate')
            ->attr(['data-url' => $url])
            ->className('bg-teal my-1 my-md-0')
            ->action('return dashboard.statusActiveSelectedAction(e, dt, node, config);');
    }

    protected static function buttonSelectDeactivate($url)
    {
        return Button::make()
            ->extend('selected')
            ->text('Deactivate')
            ->attr(['data-url' => $url])
            ->className('btn-warning my-1 my-md-0')
            ->action('return dashboard.statusDeactivateSelectedAction(e, dt, node, config);');
    }

    protected static function buttonSelectDestroyAll($url)
    {
        return Button::make()
            ->extend('selected')
            ->text('Delete')
            ->attr(['data-url' => $url])
            ->className('btn-danger my-1 my-md-0')
            ->action('return dashboard.deleteSelectedAction(e, dt, node, config);');
    }
}
